angular.module('starter.controllers', [])

.controller('DashCtrl', function($scope, $rootScope) {
	
	
	
	
	
	
	var getUserMedia = require('getusermedia')

getUserMedia({ video: true, audio: false }, function (err, stream) {
  if (err) return console.error(err)

  var Peer = require('simple-peer')
  var peer = new Peer({
    initiator: location.hash === '#init',
    trickle: false,
    stream: stream
  })

  peer.on('signal', function (data) {
    document.getElementById('yourId').value = JSON.stringify(data)
  })

  document.getElementById('connect').addEventListener('click', function () {
    var otherId = JSON.parse(document.getElementById('otherId').value)
    peer.signal(otherId)
  })

  document.getElementById('send').addEventListener('click', function () {
    var yourMessage = document.getElementById('yourMessage').value
    peer.send(yourMessage)
  })

  peer.on('data', function (data) {
    document.getElementById('messages').textContent += data + '\n'
  })

  peer.on('stream', function (stream) {
    var video = document.createElement('video')
    document.body.appendChild(video)

    video.src = window.URL.createObjectURL(stream)
    video.play()
  })
})

	
	
	
	
	
	
	
	
	
	
	
	
	$scope.dato={};
	$scope.datoss = function(){
		console.log($scope.dato);
		console.log($scope.dato.ls);
		$rootScope.e = $scope.dato.es;
		$rootScope.i = $scope.dato.is;
		$rootScope.l = $scope.dato.ls;
		$rootScope.a = $scope.dato.as;
		
			$rootScope.k1=((4*$rootScope.e*$rootScope.i)/$rootScope.l)/10000;
			$rootScope.k2=((2*$rootScope.e*$rootScope.i)/$rootScope.l)/10000;
			$rootScope.k3=(($rootScope.a*$rootScope.e)/$rootScope.l);
		var ang = $scope.dato.ang;
		$rootScope.ang = (ang * Math.PI)/180;
		//console.log('rad'+ $rootScope.ang);
		//console.log(Math.cos($rootScope.ang).toFixed(4));
		$rootScope.sin=Math.sin($rootScope.ang).toFixed(4);
		$rootScope.cos=Math.cos($rootScope.ang).toFixed(4);
		console.log($rootScope.cos);
		console.log($rootScope.sin);
	};
	$scope.datoss2 = function(){

		$rootScope.e2 = $scope.dato2.es;
		$rootScope.i2 = $scope.dato2.is;
		$rootScope.l2 = $scope.dato2.ls;
		$rootScope.a2 = $scope.dato2.as;
		
			$rootScope.k12=((4*$rootScope.e2*$rootScope.i2)/$rootScope.l2)/10000;
			$rootScope.k22=((2*$rootScope.e2*$rootScope.i2)/$rootScope.l2)/10000;
			$rootScope.k32=(($rootScope.a2*$rootScope.e2)/$rootScope.l2);
		var ang2 = $scope.dato2.ang;
		$rootScope.ang2 = (ang2 * Math.PI)/180;
		//console.log('rad'+ $rootScope.ang);
		//console.log(Math.cos($rootScope.ang).toFixed(4));
		$rootScope.sin2=Math.sin($rootScope.ang2).toFixed(4);
		$rootScope.cos2=Math.cos($rootScope.ang2).toFixed(4);
		console.log($rootScope.cos2);
		console.log($rootScope.sin2);
	};
	$scope.datoss3 = function(){

		$rootScope.e3 = $scope.dato3.es;
		$rootScope.i3 = $scope.dato3.is;
		$rootScope.l3 = $scope.dato3.ls;
		$rootScope.a3 = $scope.dato3.as;
		
			$rootScope.k13=((4*$rootScope.e3*$rootScope.i3)/$rootScope.l3)/10000;
			$rootScope.k23=((2*$rootScope.e3*$rootScope.i3)/$rootScope.l3)/10000;
			$rootScope.k33=(($rootScope.a3*$rootScope.e3)/$rootScope.l3);
		var ang3 = $scope.dato3.ang;
		$rootScope.ang3 = (ang3 * Math.PI)/180;
		//console.log('rad'+ $rootScope.ang);
		//console.log(Math.cos($rootScope.ang).toFixed(4));
		$rootScope.sin3=Math.sin($rootScope.ang3).toFixed(4);
		$rootScope.cos3=Math.cos($rootScope.ang3).toFixed(4);
		console.log($rootScope.cos3);
		console.log($rootScope.sin3);
	};
})

.controller('ChatsCtrl', function($scope, Chats, $rootScope) {
  var e,i,l,a;
  e=$rootScope.e;
  i=$rootScope.i;
  l=$rootScope.l;
  a=$rootScope.a;
  
$rootScope.a=[ 
  [0,-0.333,0], 
  [1,-0.333,0],
  [0,0,1],
  [1,0,-0.200],
  [0,0,-0.200],
  [0,1,0],
  [0,0,0],
  [0,0,0],
  [0,0,0],
  [0,0,0]];
  $scope.set = function(chat) { 
   $rootScope.aa2=chat;
   console.log(chat);
  };
$scope.k = [
  [$rootScope.k1,$rootScope.k2,0],
  [$rootScope.k2,$rootScope.k1,0],
  [0,0,$rootScope.k3]
  ];
  $scope.chats = Chats.all();
  $scope.remove = function(chat) { 
    Chats.remove(chat);
  };
})

.controller('ChatDetailCtrl', function($scope, $stateParams, Chats, $rootScope) {
  $scope.chat = Chats.get($stateParams.chatId);
  var e,i,l,sen,cos,a;
  e=$rootScope.e;
  i=$rootScope.i;
  l=$rootScope.l;
  a=$rootScope.a;
  sen=$rootScope.sin;
  cos=$rootScope.cos;
  console.log($stateParams.chatId);
  if($stateParams.chatId == '0'){
  //$scope.d1 = function() {
	$scope.q1= (4*(e*i/10000))/l;
	$scope.q2= (2*e*i/10000)/l;
	$scope.q3= ((6*e*i/10000)/(l*l))*cos;
	$scope.q4= -((6*e*i/10000)/(l*l))*cos;
	$scope.q5= -((6*e*i/10000)/(l*l))*sen;
	$scope.q6= ((6*e*i/10000)/(l*l))*sen;
	console.log($scope.q1);
	console.log($scope.q2);
	console.log($scope.q3);
	console.log($scope.q4);
	console.log($scope.q5);
	console.log($scope.q6);
  //};
  };
    if($stateParams.chatId == '1'){
    //$scope.d2 = function() {
		console.log(e*i/(10000)); 
	$scope.q1= (2*e*i/10000)/l;
	$scope.q2= (4*e*i/10000)/l;
	$scope.q3= ((6*e*i/10000)/(l*l))*cos;
	$scope.q4= -((6*e*i/10000)/(l*l))*cos; 
	$scope.q5= -((6*e*i/10000)/(l*l))*sen; 
	$scope.q6= ((6*e*i/10000)/(l*l))*sen;
	console.log($scope.q1);
	console.log($scope.q2);
	console.log($scope.q3);
	console.log($scope.q4);
	console.log($scope.q5);
	console.log($scope.q6);
  //};
  };
    if($stateParams.chatId == '2'){
    //$scope.d3 = function() {
	$scope.q1= ((6*e*i/10000)/(l*l))*cos;
	$scope.q2= ((6*e*i/10000)/(l*l))*cos;
	$scope.q3= (((12*e*i/10000)/(l*l*l))*(cos*cos))+(((a*e)/l)*(sen*sen));
	$scope.q4= -(((12*e*i/10000)/(l*l*l))*(cos*cos))-(((a*e)/l)*(sen*sen));
	$scope.q5= -(((12*e*i/10000)/(l*l*l))*(sen*cos))+(((a*e)/l)*(cos*sen));
	$scope.q6= (((12*e*i/10000)/(l*l*l))*(sen*cos))-(((a*e)/l)*(cos*sen));
	console.log($scope.q1);
	console.log($scope.q2);
	console.log($scope.q3);
	console.log($scope.q4);
	console.log($scope.q5);
	console.log($scope.q6);
  //};
  };
    if($stateParams.chatId == '3'){
    //$scope.d4 = function() {
	$scope.q1= -((6*e*i/10000)/(l*l))*cos;
	$scope.q2= -((6*e*i/10000)/(l*l))*cos;
	$scope.q3= -(((12*e*i/10000)/(l*l*l))*(cos*cos))-(((a*e)/l)*(sen*sen));
	$scope.q4= (((12*e*i/10000)/(l*l*l))*(cos*cos))+(((a*e)/l)*(sen*sen));
	$scope.q5= (((12*e*i/10000)/(l*l*l))*(sen*cos))-(((a*e)/l)*(cos*sen));
	$scope.q6= -(((12*e*i/10000)/(l*l*l))*(sen*cos))+(((a*e)/l)*(cos*sen));
	console.log($scope.q1);
	console.log($scope.q2);
	console.log($scope.q3);
	console.log($scope.q4);
	console.log($scope.q5);
	console.log($scope.q6);
  //};
  };
    if($stateParams.chatId == '4'){
    //$scope.d5 = function() {
	$scope.q1= -((6*e*i/10000)/(l*l))*sen;
	$scope.q2= -((6*e*i/10000)/(l*l))*sen;
	$scope.q3= -(((12*e*i/10000)/(l*l*l))*(sen*cos))+(((a*e)/l)*(cos*sen));
	$scope.q4= (((12*e*i/10000)/(l*l*l))*(sen*cos))-(((a*e)/l)*(cos*sen));
	$scope.q5= (((12*e*i/10000)/(l*l*l))*(sen*sen))+(((a*e)/l)*(cos*cos));
	$scope.q6= -(((12*e*i/10000)/(l*l*l))*(sen*sen))-(((a*e)/l)*(cos*cos));
	console.log($scope.q1);
	console.log($scope.q2);
	console.log($scope.q3);
	console.log($scope.q4);
	console.log($scope.q5);
	console.log($scope.q6);
  //};
  };
    if($stateParams.chatId == '5'){
    //$scope.d6 = function() {
	$scope.q1= ((6*e*i/10000)/(l*l))*sen;
	$scope.q2= ((6*e*i/10000)/(l*l))*sen;
	$scope.q3= (((12*e*i/10000)/(l*l*l))*(sen*cos))-(((a*e)/l)*(cos*sen));
	$scope.q4= -(((12*e*i/10000)/(l*l*l))*(sen*cos))+(((a*e)/l)*(cos*sen));
	$scope.q5= -(((12*e*i/10000)/(l*l*l))*(sen*sen))-(((a*e)/l)*(cos*cos));
	$scope.q6= (((12*e*i/10000)/(l*l*l))*(sen*sen))+(((a*e)/l)*(cos*cos));
	console.log($scope.q1);
	console.log($scope.q2); 
	console.log($scope.q3);
	console.log($scope.q4);
	console.log($scope.q5);  
	console.log($scope.q6);
  //};
  };

  
})

.controller('AccountCtrl', function($scope, $rootScope) {
  $scope.settings = {
    enableFriends: true
  };
  
  var e,i,l,sen,cos,a;
  e=$rootScope.e;
  i=$rootScope.i;
  l=$rootScope.l;
  a=$rootScope.a;
  console.log($rootScope.k3);
   
$scope.k = [
  [$rootScope.k1,$rootScope.k2,0],
  [$rootScope.k2,$rootScope.k1,0],
  [0,0,$rootScope.k3]
  ];
  $scope.k2 = [
  [$rootScope.k12,$rootScope.k22,0],
  [$rootScope.k22,$rootScope.k12,0],
  [0,0,$rootScope.k32]
  ];
  $scope.k3 = [
  [$rootScope.k13,$rootScope.k23,0], 
  [$rootScope.k23,$rootScope.k13,0],
  [0,0,$rootScope.k33]
  ];
  //var diasSemana = new Array("Lunes","Martes","Miércoles,","Jueves","Viernes","Sábado","Domingo");
  //var temperaturas_cuidades = new Array(new Array (12,10,11), new Array(5,0,2),new Array(10,8,10))
 //var arrayMuchasDimensiones = [1, ["hola", "que", "tal", ["estas", "estamos", "estoy"], ["bien", "mal"], "acabo"], 2, 5];
 //console.log(diasSemana);
  //console.log(temperaturas_cuidades);
  //console.log(arrayMuchasDimensiones[0]);
  //$scope.a={a11:1};
  $scope.a = $rootScope.aa2;
  $rootScope.a = $rootScope.aa2;
  console.log($rootScope.a);
  /*$scope.a=[
  [0,-0.333,0],
  [1,-0.333,0],
  [0,0,1],
  [1,0,-0.2],
  [0,0,-0.2],
  [0,1,0],
  [0,0,0],
  [0,0,0],
  [0,0,0],
  [0,0,0]];*/
  
  $scope.a22=[
  [5,4,3],
  [1,2,3],
  [7,5,9],
  [1,2,3],
  [7,5,9],
  [1,2,3],
  [7,5,9],
  [1,2,3],
  [7,5,9],
  [1,2,3]];
  
  $scope.a3=[
  [5,4,3],
  [1,2,3],
  [7,5,9],
  [1,2,3],
  [7,5,9],
  [1,2,3],
  [7,5,9],
  [1,2,3],
  [7,5,9],
  [1,2,3]];
  /**/
  /*
  $scope.k1=[
  [$scope.a[0][0],$scope.a[1][0],0],
  [$scope.a[0][1],$scope.a[1][1],0],
  [0,0,$scope.a[2][2]]
  ];

  $scope.k2=[
  [$scope.a2[0][0],$scope.a2[1][0],0],
  [$scope.a2[0][1],$scope.a2[1][1],0],
  [0,0,$scope.a[2][2]]
  ];

  $scope.k3=[
  [$scope.a3[0][0],$scope.a3[1][0],0],
  [$scope.a3[0][1],$scope.a3[1][1],0],
  [0,0,$scope.a3[2][2]]
  ];*/
   $scope.b=[
  [$scope.a[0][0],$scope.a[1][0],$scope.a[2][0],$scope.a[3][0],$scope.a[4][0],$scope.a[5][0],$scope.a[6][0],$scope.a[7][0],$scope.a[8][0],$scope.a[9][0]],
  [$scope.a[0][1],$scope.a[1][1],$scope.a[2][1],$scope.a[3][1],$scope.a[4][1],$scope.a[5][1],$scope.a[6][1],$scope.a[7][1],$scope.a[8][1],$scope.a[9][1]],
  [$scope.a[0][2],$scope.a[1][2],$scope.a[2][2],$scope.a[3][2],$scope.a[4][2],$scope.a[5][2],$scope.a[6][2],$scope.a[7][2],$scope.a[8][2],$scope.a[9][2]],
  [$scope.a[0][3],$scope.a[1][3],$scope.a[2][3],$scope.a[3][3],$scope.a[4][3],$scope.a[5][3],$scope.a[6][3],$scope.a[7][3],$scope.a[8][3],$scope.a[9][3]],
  [$scope.a[0][4],$scope.a[1][4],$scope.a[2][4],$scope.a[3][4],$scope.a[4][4],$scope.a[5][4],$scope.a[6][4],$scope.a[7][4],$scope.a[8][4],$scope.a[9][4]],
  [$scope.a[0][5],$scope.a[1][5],$scope.a[2][5],$scope.a[3][5],$scope.a[4][5],$scope.a[5][5],$scope.a[6][5],$scope.a[7][5],$scope.a[8][5],$scope.a[9][5]],
  [$scope.a[0][6],$scope.a[1][6],$scope.a[2][6],$scope.a[3][6],$scope.a[4][6],$scope.a[5][6],$scope.a[6][6],$scope.a[7][6],$scope.a[8][6],$scope.a[9][6]],
  [$scope.a[0][7],$scope.a[1][7],$scope.a[2][7],$scope.a[3][7],$scope.a[4][7],$scope.a[5][7],$scope.a[6][7],$scope.a[7][7],$scope.a[8][7],$scope.a[9][7]],
  [$scope.a[0][8],$scope.a[1][8],$scope.a[2][8],$scope.a[3][8],$scope.a[4][8],$scope.a[5][8],$scope.a[6][8],$scope.a[7][8],$scope.a[8][8],$scope.a[9][8]],
  [$scope.a[0][9],$scope.a[1][9],$scope.a[2][9],$scope.a[3][9],$scope.a[4][9],$scope.a[5][9],$scope.a[6][9],$scope.a[7][9],$scope.a[8][9],$scope.a[9][9]]
  ];/*
  $scope.b2=[
  [$scope.a2[0][0],$scope.a2[1][0],$scope.a2[2][0],$scope.a2[3][0],$scope.a2[4][0],$scope.a2[5][0],$scope.a2[6][0],$scope.a2[7][0],$scope.a2[8][0],$scope.a2[9][0]],
  [$scope.a2[0][1],$scope.a2[1][1],$scope.a2[2][1],$scope.a2[3][1],$scope.a2[4][1],$scope.a2[5][1],$scope.a2[6][1],$scope.a2[7][1],$scope.a2[8][1],$scope.a2[9][1]],
  [$scope.a2[0][2],$scope.a2[1][2],$scope.a2[2][2],$scope.a2[3][2],$scope.a2[4][2],$scope.a2[5][2],$scope.a2[6][2],$scope.a2[7][2],$scope.a2[8][2],$scope.a2[9][2]],
  [$scope.a2[0][3],$scope.a2[1][3],$scope.a2[2][3],$scope.a2[3][3],$scope.a2[4][3],$scope.a2[5][3],$scope.a2[6][3],$scope.a2[7][3],$scope.a2[8][3],$scope.a2[9][3]],
  [$scope.a2[0][4],$scope.a2[1][4],$scope.a2[2][4],$scope.a2[3][4],$scope.a2[4][4],$scope.a2[5][4],$scope.a2[6][4],$scope.a2[7][4],$scope.a2[8][4],$scope.a2[9][4]],
  [$scope.a2[0][5],$scope.a2[1][5],$scope.a2[2][5],$scope.a2[3][5],$scope.a2[4][5],$scope.a2[5][5],$scope.a2[6][5],$scope.a2[7][5],$scope.a2[8][5],$scope.a2[9][5]],
  [$scope.a2[0][6],$scope.a2[1][6],$scope.a2[2][6],$scope.a2[3][6],$scope.a2[4][6],$scope.a2[5][6],$scope.a2[6][6],$scope.a2[7][6],$scope.a2[8][6],$scope.a2[9][6]],
  [$scope.a2[0][7],$scope.a2[1][7],$scope.a2[2][7],$scope.a2[3][7],$scope.a2[4][7],$scope.a2[5][7],$scope.a2[6][7],$scope.a2[7][7],$scope.a2[8][7],$scope.a2[9][7]],
  [$scope.a2[0][8],$scope.a2[1][8],$scope.a2[2][8],$scope.a2[3][8],$scope.a2[4][8],$scope.a2[5][8],$scope.a2[6][8],$scope.a2[7][8],$scope.a2[8][8],$scope.a2[9][8]],
  [$scope.a2[0][9],$scope.a2[1][9],$scope.a2[2][9],$scope.a2[3][9],$scope.a2[4][9],$scope.a2[5][9],$scope.a2[6][9],$scope.a2[7][9],$scope.a2[8][9],$scope.a2[9][9]]
  ];
  $scope.b3=[
  [$scope.a3[0][0],$scope.a3[1][0],$scope.a3[2][0],$scope.a3[3][0],$scope.a3[4][0],$scope.a3[5][0],$scope.a3[6][0],$scope.a3[7][0],$scope.a3[8][0],$scope.a3[9][0]],
  [$scope.a3[0][1],$scope.a3[1][1],$scope.a3[2][1],$scope.a3[3][1],$scope.a3[4][1],$scope.a3[5][1],$scope.a3[6][1],$scope.a3[7][1],$scope.a3[8][1],$scope.a3[9][1]],
  [$scope.a3[0][2],$scope.a3[1][2],$scope.a3[2][2],$scope.a3[3][2],$scope.a3[4][2],$scope.a3[5][2],$scope.a3[6][2],$scope.a3[7][2],$scope.a3[8][2],$scope.a3[9][2]],
  [$scope.a3[0][3],$scope.a3[1][3],$scope.a3[2][3],$scope.a3[3][3],$scope.a3[4][3],$scope.a3[5][3],$scope.a3[6][3],$scope.a3[7][3],$scope.a3[8][3],$scope.a3[9][3]],
  [$scope.a3[0][4],$scope.a3[1][4],$scope.a3[2][4],$scope.a3[3][4],$scope.a3[4][4],$scope.a3[5][4],$scope.a3[6][4],$scope.a3[7][4],$scope.a3[8][4],$scope.a3[9][4]],
  [$scope.a3[0][5],$scope.a3[1][5],$scope.a3[2][5],$scope.a3[3][5],$scope.a3[4][5],$scope.a3[5][5],$scope.a3[6][5],$scope.a3[7][5],$scope.a3[8][5],$scope.a3[9][5]],
  [$scope.a3[0][6],$scope.a3[1][6],$scope.a3[2][6],$scope.a3[3][6],$scope.a3[4][6],$scope.a3[5][6],$scope.a3[6][6],$scope.a3[7][6],$scope.a3[8][6],$scope.a3[9][6]],
  [$scope.a3[0][7],$scope.a3[1][7],$scope.a3[2][7],$scope.a3[3][7],$scope.a3[4][7],$scope.a3[5][7],$scope.a3[6][7],$scope.a3[7][7],$scope.a3[8][7],$scope.a3[9][7]],
  [$scope.a3[0][8],$scope.a3[1][8],$scope.a3[2][8],$scope.a3[3][8],$scope.a3[4][8],$scope.a3[5][8],$scope.a3[6][8],$scope.a3[7][8],$scope.a3[8][8],$scope.a3[9][8]],
  [$scope.a3[0][9],$scope.a3[1][9],$scope.a3[2][9],$scope.a3[3][9],$scope.a3[4][9],$scope.a3[5][9],$scope.a3[6][9],$scope.a3[7][9],$scope.a3[8][9],$scope.a3[9][9]]
  ];
  */
  
  /*$scope.c=[
  [$scope.a[0][0],$scope.a[0][1],$scope.a[0][2],0,0,0,0,0,0],
  [$scope.a[0][0],$scope.a[0][1],$scope.a[0][2],0,0,0,0,0,0],
  [$scope.a[0][0],$scope.a[0][1],$scope.a[0][2],0,0,0,0,0,0],
  [0,0,0,$scope.a2[0][0],$scope.a2[0][1],$scope.a2[0][2],0,0,0],
  [0,0,0,$scope.a2[0][0],$scope.a2[0][1],$scope.a2[0][2],0,0,0],
  [0,0,0,$scope.a2[0][0],$scope.a2[0][1],$scope.a2[0][2],0,0,0],
  [0,0,0,0,0,0,$scope.a3[0][0],$scope.a3[0][1],$scope.a3[0][2]],
  [0,0,0,0,0,0,$scope.a3[0][0],$scope.a3[0][1],$scope.a3[0][2]],
  [0,0,0,0,0,0,$scope.a3[0][0],$scope.a3[0][1],$scope.a3[0][2]]
  ];*/
  $scope.a2=[
  [$scope.k[0][0],$scope.k[0][1],$scope.k[0][2],0,0,0,0,0,0],
  [$scope.k[1][0],$scope.k[1][1],$scope.k[1][2],0,0,0,0,0,0],
  [$scope.k[2][0],$scope.k[2][1],$scope.k[2][2],0,0,0,0,0,0],
  [0,0,0,$scope.k2[0][0],$scope.k2[0][1],$scope.k2[0][2],0,0,0],
  [0,0,0,$scope.k2[1][0],$scope.k2[1][1],$scope.k2[1][2],0,0,0],
  [0,0,0,$scope.k2[2][0],$scope.k2[2][1],$scope.k2[2][2],0,0,0],
  [0,0,0,0,0,0,$scope.k3[0][0],$scope.k3[0][1],$scope.k3[0][2]],
  [0,0,0,0,0,0,$scope.k3[0][0],$scope.k3[0][1],$scope.k3[0][2]],
  [0,0,0,0,0,0,$scope.k3[0][0],$scope.k3[0][1],$scope.k3[0][2]]
  ];
  /*
  $scope.b3=[
  [($scope.a2[0][0]*$scope.a2[0][0])+(),$scope.a2[0][1],$scope.a2[0][2],0,0,0,0,0,0],
  [$scope.a2[1][0],$scope.a2[1][1],$scope.a2[1][2],0,0,0,0,0,0],
  [$scope.a2[2][0],$scope.a2[2][1],$scope.a2[2][2],0,0,0,0,0,0],
  [0,0,0,$scope.a2[3][3],$scope.a2[3][4],$scope.a2[3][5],0,0,0],
  [0,0,0,$scope.a2[4][3],$scope.a2[4][4],$scope.a2[4][5],0,0,0],
  [0,0,0,$scope.a2[5][3],$scope.a2[5][4],$scope.a2[5][5],0,0,0],
  [0,0,0,0,0,0,$scope.a2[6][6],$scope.a2[6][7],$scope.a2[6][8]],
  [0,0,0,0,0,0,$scope.a2[7][6],$scope.a2[7][7],$scope.a2[7][8]],
  [0,0,0,0,0,0,$scope.a2[8][6],$scope.a2[8][7],$scope.a2[8][8]]
  ];*/
  //$scope.b3[0][0]=0;
  var b00,b01,b02,b03,b04,b05,b06,b07,b08,b09;
  var b10,b11,b12,b13,b14,b15,b16,b17,b18,b19;
  var b20,b21,b22,b23,b24,b25,b26,b27,b28,b29;
  var b30,b31,b32,b33,b34,b35,b36,b37,b38,b39;
  var b40,b41,b42,b43,b44,b45,b46,b47,b48,b49;
  var b50,b51,b52,b53,b54,b55,b56,b57,b58,b59;
  var b60,b61,b62,b63,b64,b65,b66,b67,b68,b69;
  var b70,b71,b72,b73,b74,b75,b76,b77,b78,b79;
  var b80,b81,b82,b83,b84,b85,b86,b87,b88,b89;
  var b90,b91,b92,b93,b94,b95,b96,b97,b98,b99;
  
  b00 = (
  ($scope.b[0][0]*$scope.a2[0][0])+
  ($scope.b[0][1]*$scope.a2[1][0])+
  ($scope.b[0][2]*$scope.a2[2][0])+
  ($scope.b[0][3]*$scope.a2[3][0])+
  ($scope.b[0][4]*$scope.a2[4][0])+
  ($scope.b[0][5]*$scope.a2[5][0])+
  ($scope.b[0][6]*$scope.a2[6][0])+
  ($scope.b[0][7]*$scope.a2[7][0])+
  ($scope.b[0][8]*$scope.a2[8][0]));
  
  b01 = (
  ($scope.b[0][0]*$scope.a2[0][1])+
  ($scope.b[0][1]*$scope.a2[1][1])+
  ($scope.b[0][2]*$scope.a2[2][1])+
  ($scope.b[0][3]*$scope.a2[3][1])+
  ($scope.b[0][4]*$scope.a2[4][1])+
  ($scope.b[0][5]*$scope.a2[5][1])+
  ($scope.b[0][6]*$scope.a2[6][1])+
  ($scope.b[0][7]*$scope.a2[7][1])+
  ($scope.b[0][8]*$scope.a2[8][1]));
  
  b02 = (
  ($scope.b[0][0]*$scope.a2[0][2])+
  ($scope.b[0][1]*$scope.a2[1][2])+
  ($scope.b[0][2]*$scope.a2[2][2])+
  ($scope.b[0][3]*$scope.a2[3][2])+
  ($scope.b[0][4]*$scope.a2[4][2])+
  ($scope.b[0][5]*$scope.a2[5][2])+
  ($scope.b[0][6]*$scope.a2[6][2])+
  ($scope.b[0][7]*$scope.a2[7][2])+
  ($scope.b[0][8]*$scope.a2[8][2]));
  
  b03 = (
  ($scope.b[0][0]*$scope.a2[0][3])+
  ($scope.b[0][1]*$scope.a2[1][3])+
  ($scope.b[0][2]*$scope.a2[2][3])+
  ($scope.b[0][3]*$scope.a2[3][3])+
  ($scope.b[0][4]*$scope.a2[4][3])+
  ($scope.b[0][5]*$scope.a2[5][3])+
  ($scope.b[0][6]*$scope.a2[6][3])+
  ($scope.b[0][7]*$scope.a2[7][3])+
  ($scope.b[0][8]*$scope.a2[8][3]));
  
  b04 = (
  ($scope.b[0][0]*$scope.a2[0][4])+
  ($scope.b[0][1]*$scope.a2[1][4])+
  ($scope.b[0][2]*$scope.a2[2][4])+
  ($scope.b[0][3]*$scope.a2[3][4])+
  ($scope.b[0][4]*$scope.a2[4][4])+
  ($scope.b[0][5]*$scope.a2[5][4])+
  ($scope.b[0][6]*$scope.a2[6][4])+
  ($scope.b[0][7]*$scope.a2[7][4])+
  ($scope.b[0][8]*$scope.a2[8][4]));
  
  b05 = (
  ($scope.b[0][0]*$scope.a2[0][5])+
  ($scope.b[0][1]*$scope.a2[1][5])+
  ($scope.b[0][2]*$scope.a2[2][5])+
  ($scope.b[0][3]*$scope.a2[3][5])+
  ($scope.b[0][4]*$scope.a2[4][5])+
  ($scope.b[0][5]*$scope.a2[5][5])+
  ($scope.b[0][6]*$scope.a2[6][5])+
  ($scope.b[0][7]*$scope.a2[7][5])+
  ($scope.b[0][8]*$scope.a2[8][5]));
  
  b06 = (
  ($scope.b[0][0]*$scope.a2[0][6])+
  ($scope.b[0][1]*$scope.a2[1][6])+
  ($scope.b[0][2]*$scope.a2[2][6])+
  ($scope.b[0][3]*$scope.a2[3][6])+
  ($scope.b[0][4]*$scope.a2[4][6])+
  ($scope.b[0][5]*$scope.a2[5][6])+
  ($scope.b[0][6]*$scope.a2[6][6])+
  ($scope.b[0][7]*$scope.a2[7][6])+
  ($scope.b[0][8]*$scope.a2[8][6]));
  
  b07 = (
  ($scope.b[0][0]*$scope.a2[0][7])+
  ($scope.b[0][1]*$scope.a2[1][7])+
  ($scope.b[0][2]*$scope.a2[2][7])+
  ($scope.b[0][3]*$scope.a2[3][7])+
  ($scope.b[0][4]*$scope.a2[4][7])+
  ($scope.b[0][5]*$scope.a2[5][7])+
  ($scope.b[0][6]*$scope.a2[6][7])+
  ($scope.b[0][7]*$scope.a2[7][7])+
  ($scope.b[0][8]*$scope.a2[8][7]));
  
  b08 = (
  ($scope.b[0][0]*$scope.a2[0][8])+
  ($scope.b[0][1]*$scope.a2[1][8])+
  ($scope.b[0][2]*$scope.a2[2][8])+
  ($scope.b[0][3]*$scope.a2[3][8])+
  ($scope.b[0][4]*$scope.a2[4][8])+
  ($scope.b[0][5]*$scope.a2[5][8])+
  ($scope.b[0][6]*$scope.a2[6][8])+
  ($scope.b[0][7]*$scope.a2[7][8])+
  ($scope.b[0][8]*$scope.a2[8][8]));
  
  b10 = (
  ($scope.b[1][0]*$scope.a2[0][0])+
  ($scope.b[1][1]*$scope.a2[1][0])+
  ($scope.b[1][2]*$scope.a2[2][0])+
  ($scope.b[1][3]*$scope.a2[3][0])+
  ($scope.b[1][4]*$scope.a2[4][0])+
  ($scope.b[1][5]*$scope.a2[5][0])+
  ($scope.b[1][6]*$scope.a2[6][0])+
  ($scope.b[1][7]*$scope.a2[7][0])+
  ($scope.b[1][8]*$scope.a2[8][0]));
  
  b11 = (
  ($scope.b[1][0]*$scope.a2[0][1])+
  ($scope.b[1][1]*$scope.a2[1][1])+
  ($scope.b[1][2]*$scope.a2[2][1])+
  ($scope.b[1][3]*$scope.a2[3][1])+
  ($scope.b[1][4]*$scope.a2[4][1])+
  ($scope.b[1][5]*$scope.a2[5][1])+
  ($scope.b[1][6]*$scope.a2[6][1])+
  ($scope.b[1][7]*$scope.a2[7][1])+
  ($scope.b[1][8]*$scope.a2[8][1]));
  
  b12 = (
  ($scope.b[1][0]*$scope.a2[0][2])+
  ($scope.b[1][1]*$scope.a2[1][2])+
  ($scope.b[1][2]*$scope.a2[2][2])+
  ($scope.b[1][3]*$scope.a2[3][2])+
  ($scope.b[1][4]*$scope.a2[4][2])+
  ($scope.b[1][5]*$scope.a2[5][2])+
  ($scope.b[1][6]*$scope.a2[6][2])+
  ($scope.b[1][7]*$scope.a2[7][2])+
  ($scope.b[1][8]*$scope.a2[8][2]));
  
  b13 = (
  ($scope.b[1][0]*$scope.a2[0][3])+
  ($scope.b[1][1]*$scope.a2[1][3])+
  ($scope.b[1][2]*$scope.a2[2][3])+
  ($scope.b[1][3]*$scope.a2[3][3])+
  ($scope.b[1][4]*$scope.a2[4][3])+
  ($scope.b[1][5]*$scope.a2[5][3])+
  ($scope.b[1][6]*$scope.a2[6][3])+
  ($scope.b[1][7]*$scope.a2[7][3])+
  ($scope.b[1][8]*$scope.a2[8][3]));
  
  b14 = (
  ($scope.b[1][0]*$scope.a2[0][4])+
  ($scope.b[1][1]*$scope.a2[1][4])+
  ($scope.b[1][2]*$scope.a2[2][4])+
  ($scope.b[1][3]*$scope.a2[3][4])+
  ($scope.b[1][4]*$scope.a2[4][4])+
  ($scope.b[1][5]*$scope.a2[5][4])+
  ($scope.b[1][6]*$scope.a2[6][4])+
  ($scope.b[1][7]*$scope.a2[7][4])+
  ($scope.b[1][8]*$scope.a2[8][4]));
  
  b15 = (
  ($scope.b[1][0]*$scope.a2[0][5])+
  ($scope.b[1][1]*$scope.a2[1][5])+
  ($scope.b[1][2]*$scope.a2[2][5])+
  ($scope.b[1][3]*$scope.a2[3][5])+
  ($scope.b[1][4]*$scope.a2[4][5])+
  ($scope.b[1][5]*$scope.a2[5][5])+
  ($scope.b[1][6]*$scope.a2[6][5])+
  ($scope.b[1][7]*$scope.a2[7][5])+
  ($scope.b[1][8]*$scope.a2[8][5]));
  
  b16 = (
  ($scope.b[1][0]*$scope.a2[0][6])+
  ($scope.b[1][1]*$scope.a2[1][6])+
  ($scope.b[1][2]*$scope.a2[2][6])+
  ($scope.b[1][3]*$scope.a2[3][6])+
  ($scope.b[1][4]*$scope.a2[4][6])+
  ($scope.b[1][5]*$scope.a2[5][6])+
  ($scope.b[1][6]*$scope.a2[6][6])+
  ($scope.b[1][7]*$scope.a2[7][6])+
  ($scope.b[1][8]*$scope.a2[8][6]));
  
  b17 = (
  ($scope.b[1][0]*$scope.a2[0][7])+
  ($scope.b[1][1]*$scope.a2[1][7])+
  ($scope.b[1][2]*$scope.a2[2][7])+
  ($scope.b[1][3]*$scope.a2[3][7])+
  ($scope.b[1][4]*$scope.a2[4][7])+
  ($scope.b[1][5]*$scope.a2[5][7])+
  ($scope.b[1][6]*$scope.a2[6][7])+
  ($scope.b[1][7]*$scope.a2[7][7])+
  ($scope.b[1][8]*$scope.a2[8][7]));
  
  b18 = (
  ($scope.b[1][0]*$scope.a2[0][8])+
  ($scope.b[1][1]*$scope.a2[1][8])+
  ($scope.b[1][2]*$scope.a2[2][8])+
  ($scope.b[1][3]*$scope.a2[3][8])+
  ($scope.b[1][4]*$scope.a2[4][8])+
  ($scope.b[1][5]*$scope.a2[5][8])+
  ($scope.b[1][6]*$scope.a2[6][8])+
  ($scope.b[1][7]*$scope.a2[7][8])+
  ($scope.b[1][8]*$scope.a2[8][8]));
  
  b20 = (
  ($scope.b[2][0]*$scope.a2[0][0])+
  ($scope.b[2][1]*$scope.a2[1][0])+
  ($scope.b[2][2]*$scope.a2[2][0])+
  ($scope.b[2][3]*$scope.a2[3][0])+
  ($scope.b[2][4]*$scope.a2[4][0])+
  ($scope.b[2][5]*$scope.a2[5][0])+
  ($scope.b[2][6]*$scope.a2[6][0])+
  ($scope.b[2][7]*$scope.a2[7][0])+
  ($scope.b[2][8]*$scope.a2[8][0]));
  
  b21 = (
  ($scope.b[2][0]*$scope.a2[0][1])+
  ($scope.b[2][1]*$scope.a2[1][1])+
  ($scope.b[2][2]*$scope.a2[2][1])+
  ($scope.b[2][3]*$scope.a2[3][1])+
  ($scope.b[2][4]*$scope.a2[4][1])+
  ($scope.b[2][5]*$scope.a2[5][1])+
  ($scope.b[2][6]*$scope.a2[6][1])+
  ($scope.b[2][7]*$scope.a2[7][1])+
  ($scope.b[2][8]*$scope.a2[8][1]));
  
  b22 = (
  ($scope.b[2][0]*$scope.a2[0][2])+
  ($scope.b[2][1]*$scope.a2[1][2])+
  ($scope.b[2][2]*$scope.a2[2][2])+
  ($scope.b[2][3]*$scope.a2[3][2])+
  ($scope.b[2][4]*$scope.a2[4][2])+
  ($scope.b[2][5]*$scope.a2[5][2])+
  ($scope.b[2][6]*$scope.a2[6][2])+
  ($scope.b[2][7]*$scope.a2[7][2])+
  ($scope.b[2][8]*$scope.a2[8][2]));
  
  b23 = (
  ($scope.b[2][0]*$scope.a2[0][3])+
  ($scope.b[2][1]*$scope.a2[1][3])+
  ($scope.b[2][2]*$scope.a2[2][3])+
  ($scope.b[2][3]*$scope.a2[3][3])+
  ($scope.b[2][4]*$scope.a2[4][3])+
  ($scope.b[2][5]*$scope.a2[5][3])+
  ($scope.b[2][6]*$scope.a2[6][3])+
  ($scope.b[2][7]*$scope.a2[7][3])+
  ($scope.b[2][8]*$scope.a2[8][3]));
  
  b24 = (
  ($scope.b[2][0]*$scope.a2[0][4])+
  ($scope.b[2][1]*$scope.a2[1][4])+
  ($scope.b[2][2]*$scope.a2[2][4])+
  ($scope.b[2][3]*$scope.a2[3][4])+
  ($scope.b[2][4]*$scope.a2[4][4])+
  ($scope.b[2][5]*$scope.a2[5][4])+
  ($scope.b[2][6]*$scope.a2[6][4])+
  ($scope.b[2][7]*$scope.a2[7][4])+
  ($scope.b[2][8]*$scope.a2[8][4]));
  
  b25 = (
  ($scope.b[2][0]*$scope.a2[0][5])+
  ($scope.b[2][1]*$scope.a2[1][5])+
  ($scope.b[2][2]*$scope.a2[2][5])+
  ($scope.b[2][3]*$scope.a2[3][5])+
  ($scope.b[2][4]*$scope.a2[4][5])+
  ($scope.b[2][5]*$scope.a2[5][5])+
  ($scope.b[2][6]*$scope.a2[6][5])+
  ($scope.b[2][7]*$scope.a2[7][5])+
  ($scope.b[2][8]*$scope.a2[8][5]));
  
  b26 = (
  ($scope.b[2][0]*$scope.a2[0][6])+
  ($scope.b[2][1]*$scope.a2[1][6])+
  ($scope.b[2][2]*$scope.a2[2][6])+
  ($scope.b[2][3]*$scope.a2[3][6])+
  ($scope.b[2][4]*$scope.a2[4][6])+
  ($scope.b[2][5]*$scope.a2[5][6])+
  ($scope.b[2][6]*$scope.a2[6][6])+
  ($scope.b[2][7]*$scope.a2[7][6])+
  ($scope.b[2][8]*$scope.a2[8][6]));
  
  b27 = (
  ($scope.b[2][0]*$scope.a2[0][7])+
  ($scope.b[2][1]*$scope.a2[1][7])+
  ($scope.b[2][2]*$scope.a2[2][7])+
  ($scope.b[2][3]*$scope.a2[3][7])+
  ($scope.b[2][4]*$scope.a2[4][7])+
  ($scope.b[2][5]*$scope.a2[5][7])+
  ($scope.b[2][6]*$scope.a2[6][7])+
  ($scope.b[2][7]*$scope.a2[7][7])+
  ($scope.b[2][8]*$scope.a2[8][7]));
  
  b28 = (
  ($scope.b[2][0]*$scope.a2[0][8])+
  ($scope.b[2][1]*$scope.a2[1][8])+
  ($scope.b[2][2]*$scope.a2[2][8])+
  ($scope.b[2][3]*$scope.a2[3][8])+
  ($scope.b[2][4]*$scope.a2[4][8])+
  ($scope.b[2][5]*$scope.a2[5][8])+
  ($scope.b[2][6]*$scope.a2[6][8])+
  ($scope.b[2][7]*$scope.a2[7][8])+
  ($scope.b[2][8]*$scope.a2[8][8]));
  
  b30 = (
  ($scope.b[3][0]*$scope.a2[0][0])+
  ($scope.b[3][1]*$scope.a2[1][0])+
  ($scope.b[3][2]*$scope.a2[2][0])+
  ($scope.b[3][3]*$scope.a2[3][0])+
  ($scope.b[3][4]*$scope.a2[4][0])+
  ($scope.b[3][5]*$scope.a2[5][0])+
  ($scope.b[3][6]*$scope.a2[6][0])+
  ($scope.b[3][7]*$scope.a2[7][0])+
  ($scope.b[3][8]*$scope.a2[8][0]));
  
  b31 = (
  ($scope.b[3][0]*$scope.a2[0][1])+
  ($scope.b[3][1]*$scope.a2[1][1])+
  ($scope.b[3][2]*$scope.a2[2][1])+
  ($scope.b[3][3]*$scope.a2[3][1])+
  ($scope.b[3][4]*$scope.a2[4][1])+
  ($scope.b[3][5]*$scope.a2[5][1])+
  ($scope.b[3][6]*$scope.a2[6][1])+
  ($scope.b[3][7]*$scope.a2[7][1])+
  ($scope.b[3][8]*$scope.a2[8][1]));
  
  b32 = (
  ($scope.b[3][0]*$scope.a2[0][2])+
  ($scope.b[3][1]*$scope.a2[1][2])+
  ($scope.b[3][2]*$scope.a2[2][2])+
  ($scope.b[3][3]*$scope.a2[3][2])+
  ($scope.b[3][4]*$scope.a2[4][2])+
  ($scope.b[3][5]*$scope.a2[5][2])+
  ($scope.b[3][6]*$scope.a2[6][2])+
  ($scope.b[3][7]*$scope.a2[7][2])+
  ($scope.b[3][8]*$scope.a2[8][2]));
  
  b33 = (
  ($scope.b[3][0]*$scope.a2[0][3])+
  ($scope.b[3][1]*$scope.a2[1][3])+
  ($scope.b[3][2]*$scope.a2[2][3])+
  ($scope.b[3][3]*$scope.a2[3][3])+
  ($scope.b[3][4]*$scope.a2[4][3])+
  ($scope.b[3][5]*$scope.a2[5][3])+
  ($scope.b[3][6]*$scope.a2[6][3])+
  ($scope.b[3][7]*$scope.a2[7][3])+
  ($scope.b[3][8]*$scope.a2[8][3]));
  
  b34 = (
  ($scope.b[3][0]*$scope.a2[0][4])+
  ($scope.b[3][1]*$scope.a2[1][4])+
  ($scope.b[3][2]*$scope.a2[2][4])+
  ($scope.b[3][3]*$scope.a2[3][4])+
  ($scope.b[3][4]*$scope.a2[4][4])+
  ($scope.b[3][5]*$scope.a2[5][4])+
  ($scope.b[3][6]*$scope.a2[6][4])+
  ($scope.b[3][7]*$scope.a2[7][4])+
  ($scope.b[3][8]*$scope.a2[8][4]));
  
  b35 = (
  ($scope.b[3][0]*$scope.a2[0][5])+
  ($scope.b[3][1]*$scope.a2[1][5])+
  ($scope.b[3][2]*$scope.a2[2][5])+
  ($scope.b[3][3]*$scope.a2[3][5])+
  ($scope.b[3][4]*$scope.a2[4][5])+
  ($scope.b[3][5]*$scope.a2[5][5])+
  ($scope.b[3][6]*$scope.a2[6][5])+
  ($scope.b[3][7]*$scope.a2[7][5])+
  ($scope.b[3][8]*$scope.a2[8][5]));
  
  b36 = (
  ($scope.b[3][0]*$scope.a2[0][6])+
  ($scope.b[3][1]*$scope.a2[1][6])+
  ($scope.b[3][2]*$scope.a2[2][6])+
  ($scope.b[3][3]*$scope.a2[3][6])+
  ($scope.b[3][4]*$scope.a2[4][6])+
  ($scope.b[3][5]*$scope.a2[5][6])+
  ($scope.b[3][6]*$scope.a2[6][6])+
  ($scope.b[3][7]*$scope.a2[7][6])+
  ($scope.b[3][8]*$scope.a2[8][6]));
  
  b37 = (
  ($scope.b[3][0]*$scope.a2[0][7])+
  ($scope.b[3][1]*$scope.a2[1][7])+
  ($scope.b[3][2]*$scope.a2[2][7])+
  ($scope.b[3][3]*$scope.a2[3][7])+
  ($scope.b[3][4]*$scope.a2[4][7])+
  ($scope.b[3][5]*$scope.a2[5][7])+
  ($scope.b[3][6]*$scope.a2[6][7])+
  ($scope.b[3][7]*$scope.a2[7][7])+
  ($scope.b[3][8]*$scope.a2[8][7]));
  
  b38 = (
  ($scope.b[3][0]*$scope.a2[0][8])+
  ($scope.b[3][1]*$scope.a2[1][8])+
  ($scope.b[3][2]*$scope.a2[2][8])+
  ($scope.b[3][3]*$scope.a2[3][8])+
  ($scope.b[3][4]*$scope.a2[4][8])+
  ($scope.b[3][5]*$scope.a2[5][8])+
  ($scope.b[3][6]*$scope.a2[6][8])+
  ($scope.b[3][7]*$scope.a2[7][8])+
  ($scope.b[3][8]*$scope.a2[8][8]));
  
  b40 = (
  ($scope.b[4][0]*$scope.a2[0][0])+
  ($scope.b[4][1]*$scope.a2[1][0])+
  ($scope.b[4][2]*$scope.a2[2][0])+
  ($scope.b[4][3]*$scope.a2[3][0])+
  ($scope.b[4][4]*$scope.a2[4][0])+
  ($scope.b[4][5]*$scope.a2[5][0])+
  ($scope.b[4][6]*$scope.a2[6][0])+
  ($scope.b[4][7]*$scope.a2[7][0])+
  ($scope.b[4][8]*$scope.a2[8][0]));
  
  b41 = (
  ($scope.b[4][0]*$scope.a2[0][1])+
  ($scope.b[4][1]*$scope.a2[1][1])+
  ($scope.b[4][2]*$scope.a2[2][1])+
  ($scope.b[4][3]*$scope.a2[3][1])+
  ($scope.b[4][4]*$scope.a2[4][1])+
  ($scope.b[4][5]*$scope.a2[5][1])+
  ($scope.b[4][6]*$scope.a2[6][1])+
  ($scope.b[4][7]*$scope.a2[7][1])+
  ($scope.b[4][8]*$scope.a2[8][1]));
  
  b42 = (
  ($scope.b[4][0]*$scope.a2[0][2])+
  ($scope.b[4][1]*$scope.a2[1][2])+
  ($scope.b[4][2]*$scope.a2[2][2])+
  ($scope.b[4][3]*$scope.a2[3][2])+
  ($scope.b[4][4]*$scope.a2[4][2])+
  ($scope.b[4][5]*$scope.a2[5][2])+
  ($scope.b[4][6]*$scope.a2[6][2])+
  ($scope.b[4][7]*$scope.a2[7][2])+
  ($scope.b[4][8]*$scope.a2[8][2]));
  
  b43 = (
  ($scope.b[4][0]*$scope.a2[0][3])+
  ($scope.b[4][1]*$scope.a2[1][3])+
  ($scope.b[4][2]*$scope.a2[2][3])+
  ($scope.b[4][3]*$scope.a2[3][3])+
  ($scope.b[4][4]*$scope.a2[4][3])+
  ($scope.b[4][5]*$scope.a2[5][3])+
  ($scope.b[4][6]*$scope.a2[6][3])+
  ($scope.b[4][7]*$scope.a2[7][3])+
  ($scope.b[4][8]*$scope.a2[8][3]));
  
  b44 = (
  ($scope.b[4][0]*$scope.a2[0][4])+
  ($scope.b[4][1]*$scope.a2[1][4])+
  ($scope.b[4][2]*$scope.a2[2][4])+
  ($scope.b[4][3]*$scope.a2[3][4])+
  ($scope.b[4][4]*$scope.a2[4][4])+
  ($scope.b[4][5]*$scope.a2[5][4])+
  ($scope.b[4][6]*$scope.a2[6][4])+
  ($scope.b[4][7]*$scope.a2[7][4])+
  ($scope.b[4][8]*$scope.a2[8][4]));
  
  b45 = (
  ($scope.b[4][0]*$scope.a2[0][5])+
  ($scope.b[4][1]*$scope.a2[1][5])+
  ($scope.b[4][2]*$scope.a2[2][5])+
  ($scope.b[4][3]*$scope.a2[3][5])+
  ($scope.b[4][4]*$scope.a2[4][5])+
  ($scope.b[4][5]*$scope.a2[5][5])+
  ($scope.b[4][6]*$scope.a2[6][5])+
  ($scope.b[4][7]*$scope.a2[7][5])+
  ($scope.b[4][8]*$scope.a2[8][5]));
  
  b46 = (
  ($scope.b[4][0]*$scope.a2[0][6])+
  ($scope.b[4][1]*$scope.a2[1][6])+
  ($scope.b[4][2]*$scope.a2[2][6])+
  ($scope.b[4][3]*$scope.a2[3][6])+
  ($scope.b[4][4]*$scope.a2[4][6])+
  ($scope.b[4][5]*$scope.a2[5][6])+
  ($scope.b[4][6]*$scope.a2[6][6])+
  ($scope.b[4][7]*$scope.a2[7][6])+
  ($scope.b[4][8]*$scope.a2[8][6]));
  
  b47 = (
  ($scope.b[4][0]*$scope.a2[0][7])+
  ($scope.b[4][1]*$scope.a2[1][7])+
  ($scope.b[4][2]*$scope.a2[2][7])+
  ($scope.b[4][3]*$scope.a2[3][7])+
  ($scope.b[4][4]*$scope.a2[4][7])+
  ($scope.b[4][5]*$scope.a2[5][7])+
  ($scope.b[4][6]*$scope.a2[6][7])+
  ($scope.b[4][7]*$scope.a2[7][7])+
  ($scope.b[4][8]*$scope.a2[8][7]));
  
  b48 = (
  ($scope.b[4][0]*$scope.a2[0][8])+
  ($scope.b[4][1]*$scope.a2[1][8])+
  ($scope.b[4][2]*$scope.a2[2][8])+
  ($scope.b[4][3]*$scope.a2[3][8])+
  ($scope.b[4][4]*$scope.a2[4][8])+
  ($scope.b[4][5]*$scope.a2[5][8])+
  ($scope.b[4][6]*$scope.a2[6][8])+
  ($scope.b[4][7]*$scope.a2[7][8])+
  ($scope.b[4][8]*$scope.a2[8][8]));
  
  b50 = (
  ($scope.b[5][0]*$scope.a2[0][0])+
  ($scope.b[5][1]*$scope.a2[1][0])+
  ($scope.b[5][2]*$scope.a2[2][0])+
  ($scope.b[5][3]*$scope.a2[3][0])+
  ($scope.b[5][4]*$scope.a2[4][0])+
  ($scope.b[5][5]*$scope.a2[5][0])+
  ($scope.b[5][6]*$scope.a2[6][0])+
  ($scope.b[5][7]*$scope.a2[7][0])+
  ($scope.b[5][8]*$scope.a2[8][0]));
  
   b51 = (
  ($scope.b[5][0]*$scope.a2[0][1])+
  ($scope.b[5][1]*$scope.a2[1][1])+
  ($scope.b[5][2]*$scope.a2[2][1])+
  ($scope.b[5][3]*$scope.a2[3][1])+
  ($scope.b[5][4]*$scope.a2[4][1])+
  ($scope.b[5][5]*$scope.a2[5][1])+
  ($scope.b[5][6]*$scope.a2[6][1])+
  ($scope.b[5][7]*$scope.a2[7][1])+
  ($scope.b[5][8]*$scope.a2[8][1]));
  
   b52 = (
  ($scope.b[5][0]*$scope.a2[0][2])+
  ($scope.b[5][1]*$scope.a2[1][2])+
  ($scope.b[5][2]*$scope.a2[2][2])+
  ($scope.b[5][3]*$scope.a2[3][2])+
  ($scope.b[5][4]*$scope.a2[4][2])+
  ($scope.b[5][5]*$scope.a2[5][2])+
  ($scope.b[5][6]*$scope.a2[6][2])+
  ($scope.b[5][7]*$scope.a2[7][2])+
  ($scope.b[5][8]*$scope.a2[8][2]));
  
   b53 = (
  ($scope.b[5][0]*$scope.a2[0][3])+
  ($scope.b[5][1]*$scope.a2[1][3])+
  ($scope.b[5][2]*$scope.a2[2][3])+
  ($scope.b[5][3]*$scope.a2[3][3])+
  ($scope.b[5][4]*$scope.a2[4][3])+
  ($scope.b[5][5]*$scope.a2[5][3])+
  ($scope.b[5][6]*$scope.a2[6][3])+
  ($scope.b[5][7]*$scope.a2[7][3])+
  ($scope.b[5][8]*$scope.a2[8][3]));
  
   b54 = (
  ($scope.b[5][0]*$scope.a2[0][4])+
  ($scope.b[5][1]*$scope.a2[1][4])+
  ($scope.b[5][2]*$scope.a2[2][4])+
  ($scope.b[5][3]*$scope.a2[3][4])+
  ($scope.b[5][4]*$scope.a2[4][4])+
  ($scope.b[5][5]*$scope.a2[5][4])+
  ($scope.b[5][6]*$scope.a2[6][4])+
  ($scope.b[5][7]*$scope.a2[7][4])+
  ($scope.b[5][8]*$scope.a2[8][4]));
  
   b55 = (
  ($scope.b[5][0]*$scope.a2[0][5])+
  ($scope.b[5][1]*$scope.a2[1][5])+
  ($scope.b[5][2]*$scope.a2[2][5])+
  ($scope.b[5][3]*$scope.a2[3][5])+
  ($scope.b[5][4]*$scope.a2[4][5])+
  ($scope.b[5][5]*$scope.a2[5][5])+
  ($scope.b[5][6]*$scope.a2[6][5])+
  ($scope.b[5][7]*$scope.a2[7][5])+
  ($scope.b[5][8]*$scope.a2[8][5]));
  
   b56 = (
  ($scope.b[5][0]*$scope.a2[0][6])+
  ($scope.b[5][1]*$scope.a2[1][6])+
  ($scope.b[5][2]*$scope.a2[2][6])+
  ($scope.b[5][3]*$scope.a2[3][6])+
  ($scope.b[5][4]*$scope.a2[4][6])+
  ($scope.b[5][5]*$scope.a2[5][6])+
  ($scope.b[5][6]*$scope.a2[6][6])+
  ($scope.b[5][7]*$scope.a2[7][6])+
  ($scope.b[5][8]*$scope.a2[8][6]));
  
   b57 = (
  ($scope.b[5][0]*$scope.a2[0][7])+
  ($scope.b[5][1]*$scope.a2[1][7])+
  ($scope.b[5][2]*$scope.a2[2][7])+
  ($scope.b[5][3]*$scope.a2[3][7])+
  ($scope.b[5][4]*$scope.a2[4][7])+
  ($scope.b[5][5]*$scope.a2[5][7])+
  ($scope.b[5][6]*$scope.a2[6][7])+
  ($scope.b[5][7]*$scope.a2[7][7])+
  ($scope.b[5][8]*$scope.a2[8][7]));
  
   b58 = (
  ($scope.b[5][0]*$scope.a2[0][8])+
  ($scope.b[5][1]*$scope.a2[1][8])+
  ($scope.b[5][2]*$scope.a2[2][8])+
  ($scope.b[5][3]*$scope.a2[3][8])+
  ($scope.b[5][4]*$scope.a2[4][8])+
  ($scope.b[5][5]*$scope.a2[5][8])+
  ($scope.b[5][6]*$scope.a2[6][8])+
  ($scope.b[5][7]*$scope.a2[7][8])+
  ($scope.b[5][8]*$scope.a2[8][8]));
  
   b60 = (
  ($scope.b[6][0]*$scope.a2[0][0])+
  ($scope.b[6][1]*$scope.a2[1][0])+
  ($scope.b[6][2]*$scope.a2[2][0])+
  ($scope.b[6][3]*$scope.a2[3][0])+
  ($scope.b[6][4]*$scope.a2[4][0])+
  ($scope.b[6][5]*$scope.a2[5][0])+
  ($scope.b[6][6]*$scope.a2[6][0])+
  ($scope.b[6][7]*$scope.a2[7][0])+
  ($scope.b[6][8]*$scope.a2[8][0]));
  
   b61 = (
  ($scope.b[6][0]*$scope.a2[0][1])+
  ($scope.b[6][1]*$scope.a2[1][1])+
  ($scope.b[6][2]*$scope.a2[2][1])+
  ($scope.b[6][3]*$scope.a2[3][1])+
  ($scope.b[6][4]*$scope.a2[4][1])+
  ($scope.b[6][5]*$scope.a2[5][1])+
  ($scope.b[6][6]*$scope.a2[6][1])+
  ($scope.b[6][7]*$scope.a2[7][1])+
  ($scope.b[6][8]*$scope.a2[8][1]));
  
  b62 = (
  ($scope.b[6][0]*$scope.a2[0][2])+
  ($scope.b[6][1]*$scope.a2[1][2])+
  ($scope.b[6][2]*$scope.a2[2][2])+
  ($scope.b[6][3]*$scope.a2[3][2])+
  ($scope.b[6][4]*$scope.a2[4][2])+
  ($scope.b[6][5]*$scope.a2[5][2])+
  ($scope.b[6][6]*$scope.a2[6][2])+
  ($scope.b[6][7]*$scope.a2[7][2])+
  ($scope.b[6][8]*$scope.a2[8][2]));
  
  b63 = (
  ($scope.b[6][0]*$scope.a2[0][3])+
  ($scope.b[6][1]*$scope.a2[1][3])+
  ($scope.b[6][2]*$scope.a2[2][3])+
  ($scope.b[6][3]*$scope.a2[3][3])+
  ($scope.b[6][4]*$scope.a2[4][3])+
  ($scope.b[6][5]*$scope.a2[5][3])+
  ($scope.b[6][6]*$scope.a2[6][3])+
  ($scope.b[6][7]*$scope.a2[7][3])+
  ($scope.b[6][8]*$scope.a2[8][3]));
  
  b64 = (
  ($scope.b[6][0]*$scope.a2[0][4])+
  ($scope.b[6][1]*$scope.a2[1][4])+
  ($scope.b[6][2]*$scope.a2[2][4])+
  ($scope.b[6][3]*$scope.a2[3][4])+
  ($scope.b[6][4]*$scope.a2[4][4])+
  ($scope.b[6][5]*$scope.a2[5][4])+
  ($scope.b[6][6]*$scope.a2[6][4])+
  ($scope.b[6][7]*$scope.a2[7][4])+
  ($scope.b[6][8]*$scope.a2[8][4]));
  
  b65 = (
  ($scope.b[6][0]*$scope.a2[0][5])+
  ($scope.b[6][1]*$scope.a2[1][5])+
  ($scope.b[6][2]*$scope.a2[2][5])+
  ($scope.b[6][3]*$scope.a2[3][5])+
  ($scope.b[6][4]*$scope.a2[4][5])+
  ($scope.b[6][5]*$scope.a2[5][5])+
  ($scope.b[6][6]*$scope.a2[6][5])+
  ($scope.b[6][7]*$scope.a2[7][5])+
  ($scope.b[6][8]*$scope.a2[8][5]));
  
  b66 = (
  ($scope.b[6][0]*$scope.a2[0][6])+
  ($scope.b[6][1]*$scope.a2[1][6])+
  ($scope.b[6][2]*$scope.a2[2][6])+
  ($scope.b[6][3]*$scope.a2[3][6])+
  ($scope.b[6][4]*$scope.a2[4][6])+
  ($scope.b[6][5]*$scope.a2[5][6])+
  ($scope.b[6][6]*$scope.a2[6][6])+
  ($scope.b[6][7]*$scope.a2[7][6])+
  ($scope.b[6][8]*$scope.a2[8][6]));
  
  b67 = (
  ($scope.b[6][0]*$scope.a2[0][7])+
  ($scope.b[6][1]*$scope.a2[1][7])+
  ($scope.b[6][2]*$scope.a2[2][7])+
  ($scope.b[6][3]*$scope.a2[3][7])+
  ($scope.b[6][4]*$scope.a2[4][7])+
  ($scope.b[6][5]*$scope.a2[5][7])+
  ($scope.b[6][6]*$scope.a2[6][7])+
  ($scope.b[6][7]*$scope.a2[7][7])+
  ($scope.b[6][8]*$scope.a2[8][7]));
  
  b68 = (
  ($scope.b[6][0]*$scope.a2[0][8])+
  ($scope.b[6][1]*$scope.a2[1][8])+
  ($scope.b[6][2]*$scope.a2[2][8])+
  ($scope.b[6][3]*$scope.a2[3][8])+
  ($scope.b[6][4]*$scope.a2[4][8])+
  ($scope.b[6][5]*$scope.a2[5][8])+
  ($scope.b[6][6]*$scope.a2[6][8])+
  ($scope.b[6][7]*$scope.a2[7][8])+
  ($scope.b[6][8]*$scope.a2[8][8]));
  
  b70 = (
  ($scope.b[7][0]*$scope.a2[0][0])+
  ($scope.b[7][1]*$scope.a2[1][0])+
  ($scope.b[7][2]*$scope.a2[2][0])+
  ($scope.b[7][3]*$scope.a2[3][0])+
  ($scope.b[7][4]*$scope.a2[4][0])+
  ($scope.b[7][5]*$scope.a2[5][0])+
  ($scope.b[7][6]*$scope.a2[6][0])+
  ($scope.b[7][7]*$scope.a2[7][0])+
  ($scope.b[7][8]*$scope.a2[8][0]));
  
  b71 = (
  ($scope.b[7][0]*$scope.a2[0][1])+
  ($scope.b[7][1]*$scope.a2[1][1])+
  ($scope.b[7][2]*$scope.a2[2][1])+
  ($scope.b[7][3]*$scope.a2[3][1])+
  ($scope.b[7][4]*$scope.a2[4][1])+
  ($scope.b[7][5]*$scope.a2[5][1])+
  ($scope.b[7][6]*$scope.a2[6][1])+
  ($scope.b[7][7]*$scope.a2[7][1])+
  ($scope.b[7][8]*$scope.a2[8][1]));
  
  b72 = (
  ($scope.b[7][0]*$scope.a2[0][2])+
  ($scope.b[7][1]*$scope.a2[1][2])+
  ($scope.b[7][2]*$scope.a2[2][2])+
  ($scope.b[7][3]*$scope.a2[3][2])+
  ($scope.b[7][4]*$scope.a2[4][2])+
  ($scope.b[7][5]*$scope.a2[5][2])+
  ($scope.b[7][6]*$scope.a2[6][2])+
  ($scope.b[7][7]*$scope.a2[7][2])+
  ($scope.b[7][8]*$scope.a2[8][2]));
  
  b73 = (
  ($scope.b[7][0]*$scope.a2[0][3])+
  ($scope.b[7][1]*$scope.a2[1][3])+
  ($scope.b[7][2]*$scope.a2[2][3])+
  ($scope.b[7][3]*$scope.a2[3][3])+
  ($scope.b[7][4]*$scope.a2[4][3])+
  ($scope.b[7][5]*$scope.a2[5][3])+
  ($scope.b[7][6]*$scope.a2[6][3])+
  ($scope.b[7][7]*$scope.a2[7][3])+
  ($scope.b[7][8]*$scope.a2[8][3]));
  
  b74 = (
  ($scope.b[7][0]*$scope.a2[0][4])+
  ($scope.b[7][1]*$scope.a2[1][4])+
  ($scope.b[7][2]*$scope.a2[2][4])+
  ($scope.b[7][3]*$scope.a2[3][4])+
  ($scope.b[7][4]*$scope.a2[4][4])+
  ($scope.b[7][5]*$scope.a2[5][4])+
  ($scope.b[7][6]*$scope.a2[6][4])+
  ($scope.b[7][7]*$scope.a2[7][4])+
  ($scope.b[7][8]*$scope.a2[8][4]));
  
  b75 = (
  ($scope.b[7][0]*$scope.a2[0][5])+
  ($scope.b[7][1]*$scope.a2[1][5])+
  ($scope.b[7][2]*$scope.a2[2][5])+
  ($scope.b[7][3]*$scope.a2[3][5])+
  ($scope.b[7][4]*$scope.a2[4][5])+
  ($scope.b[7][5]*$scope.a2[5][5])+
  ($scope.b[7][6]*$scope.a2[6][5])+
  ($scope.b[7][7]*$scope.a2[7][5])+
  ($scope.b[7][8]*$scope.a2[8][5]));
  
  b76 = (
  ($scope.b[7][0]*$scope.a2[0][6])+
  ($scope.b[7][1]*$scope.a2[1][6])+
  ($scope.b[7][2]*$scope.a2[2][6])+
  ($scope.b[7][3]*$scope.a2[3][6])+
  ($scope.b[7][4]*$scope.a2[4][6])+
  ($scope.b[7][5]*$scope.a2[5][6])+
  ($scope.b[7][6]*$scope.a2[6][6])+
  ($scope.b[7][7]*$scope.a2[7][6])+
  ($scope.b[7][8]*$scope.a2[8][6]));
  
  b77 = (
  ($scope.b[7][0]*$scope.a2[0][7])+
  ($scope.b[7][1]*$scope.a2[1][7])+
  ($scope.b[7][2]*$scope.a2[2][7])+
  ($scope.b[7][3]*$scope.a2[3][7])+
  ($scope.b[7][4]*$scope.a2[4][7])+
  ($scope.b[7][5]*$scope.a2[5][7])+
  ($scope.b[7][6]*$scope.a2[6][7])+
  ($scope.b[7][7]*$scope.a2[7][7])+
  ($scope.b[7][8]*$scope.a2[8][7]));
  
  b78 = (
  ($scope.b[7][0]*$scope.a2[0][8])+
  ($scope.b[7][1]*$scope.a2[1][8])+
  ($scope.b[7][2]*$scope.a2[2][8])+
  ($scope.b[7][3]*$scope.a2[3][8])+
  ($scope.b[7][4]*$scope.a2[4][8])+
  ($scope.b[7][5]*$scope.a2[5][8])+
  ($scope.b[7][6]*$scope.a2[6][8])+
  ($scope.b[7][7]*$scope.a2[7][8])+
  ($scope.b[7][8]*$scope.a2[8][8]));
  
  
  b80 = (
  ($scope.b[8][0]*$scope.a2[0][0])+
  ($scope.b[8][1]*$scope.a2[1][0])+
  ($scope.b[8][2]*$scope.a2[2][0])+
  ($scope.b[8][3]*$scope.a2[3][0])+
  ($scope.b[8][4]*$scope.a2[4][0])+
  ($scope.b[8][5]*$scope.a2[5][0])+
  ($scope.b[8][6]*$scope.a2[6][0])+
  ($scope.b[8][7]*$scope.a2[7][0])+
  ($scope.b[8][8]*$scope.a2[8][0]));
  
  b81 = (
  ($scope.b[8][0]*$scope.a2[0][1])+
  ($scope.b[8][1]*$scope.a2[1][1])+
  ($scope.b[8][2]*$scope.a2[2][1])+
  ($scope.b[8][3]*$scope.a2[3][1])+
  ($scope.b[8][4]*$scope.a2[4][1])+
  ($scope.b[8][5]*$scope.a2[5][1])+
  ($scope.b[8][6]*$scope.a2[6][1])+
  ($scope.b[8][7]*$scope.a2[7][1])+
  ($scope.b[8][8]*$scope.a2[8][1]));
  
  b82 = (
  ($scope.b[8][0]*$scope.a2[0][2])+
  ($scope.b[8][1]*$scope.a2[1][2])+
  ($scope.b[8][2]*$scope.a2[2][2])+
  ($scope.b[8][3]*$scope.a2[3][2])+
  ($scope.b[8][4]*$scope.a2[4][2])+
  ($scope.b[8][5]*$scope.a2[5][2])+
  ($scope.b[8][6]*$scope.a2[6][2])+
  ($scope.b[8][7]*$scope.a2[7][2])+
  ($scope.b[8][8]*$scope.a2[8][2]));
  
  b83 = (
  ($scope.b[8][0]*$scope.a2[0][3])+
  ($scope.b[8][1]*$scope.a2[1][3])+
  ($scope.b[8][2]*$scope.a2[2][3])+
  ($scope.b[8][3]*$scope.a2[3][3])+
  ($scope.b[8][4]*$scope.a2[4][3])+
  ($scope.b[8][5]*$scope.a2[5][3])+
  ($scope.b[8][6]*$scope.a2[6][3])+
  ($scope.b[8][7]*$scope.a2[7][3])+
  ($scope.b[8][8]*$scope.a2[8][3]));
  
  b84 = (
  ($scope.b[8][0]*$scope.a2[0][4])+
  ($scope.b[8][1]*$scope.a2[1][4])+
  ($scope.b[8][2]*$scope.a2[2][4])+
  ($scope.b[8][3]*$scope.a2[3][4])+
  ($scope.b[8][4]*$scope.a2[4][4])+
  ($scope.b[8][5]*$scope.a2[5][4])+
  ($scope.b[8][6]*$scope.a2[6][4])+
  ($scope.b[8][7]*$scope.a2[7][4])+
  ($scope.b[8][8]*$scope.a2[8][4]));
  
  b85 = (
  ($scope.b[8][0]*$scope.a2[0][5])+
  ($scope.b[8][1]*$scope.a2[1][5])+
  ($scope.b[8][2]*$scope.a2[2][5])+
  ($scope.b[8][3]*$scope.a2[3][5])+
  ($scope.b[8][4]*$scope.a2[4][5])+
  ($scope.b[8][5]*$scope.a2[5][5])+
  ($scope.b[8][6]*$scope.a2[6][5])+
  ($scope.b[8][7]*$scope.a2[7][5])+
  ($scope.b[8][8]*$scope.a2[8][5]));
  
  b86 = (
  ($scope.b[8][0]*$scope.a2[0][6])+
  ($scope.b[8][1]*$scope.a2[1][6])+
  ($scope.b[8][2]*$scope.a2[2][6])+
  ($scope.b[8][3]*$scope.a2[3][6])+
  ($scope.b[8][4]*$scope.a2[4][6])+
  ($scope.b[8][5]*$scope.a2[5][6])+
  ($scope.b[8][6]*$scope.a2[6][6])+
  ($scope.b[8][7]*$scope.a2[7][6])+
  ($scope.b[8][8]*$scope.a2[8][6]));
  
  b87 = (
  ($scope.b[8][0]*$scope.a2[0][7])+
  ($scope.b[8][1]*$scope.a2[1][7])+
  ($scope.b[8][2]*$scope.a2[2][7])+
  ($scope.b[8][3]*$scope.a2[3][7])+
  ($scope.b[8][4]*$scope.a2[4][7])+
  ($scope.b[8][5]*$scope.a2[5][7])+
  ($scope.b[8][6]*$scope.a2[6][7])+
  ($scope.b[8][7]*$scope.a2[7][7])+
  ($scope.b[8][8]*$scope.a2[8][7]));
  
  b88 = (
  ($scope.b[8][0]*$scope.a2[0][8])+
  ($scope.b[8][1]*$scope.a2[1][8])+
  ($scope.b[8][2]*$scope.a2[2][8])+
  ($scope.b[8][3]*$scope.a2[3][8])+
  ($scope.b[8][4]*$scope.a2[4][8])+
  ($scope.b[8][5]*$scope.a2[5][8])+
  ($scope.b[8][6]*$scope.a2[6][8])+
  ($scope.b[8][7]*$scope.a2[7][8])+
  ($scope.b[8][8]*$scope.a2[8][8]));
  
  
  
  console.log(b00);
  
  $scope.b3=[
  [b00,b01,b02,b03,b04,b05,b06,b07,b08],
  [b10,b11,b12,b13,b14,b15,b16,b17,b18],
  [b20,b21,b22,b23,b24,b25,b26,b27,b28],
  [b30,b31,b32,b33,b34,b35,b36,b37,b38],
  [b40,b41,b42,b43,b44,b45,b46,b47,b48],
  [b50,b51,b52,b53,b54,b55,b56,b57,b58],
  [b60,b61,b62,b63,b64,b65,b66,b67,b68],
  [b70,b71,b72,b73,b74,b75,b76,b77,b78],
  [b80,b81,b82,b83,b84,b85,b86,b87,b88]
  ];
  
  
  var c00,c01,c02,c03,c04,c05,c06,c07,c08;
  var c10,c11,c12,c13,c14,c15,c16,c17,c18;
  var c20,c21,c22,c23,c24,c25,c26,c27,c28;
  var c30,c31,c32,c33,c34,c35,c36,c37,c38;
  var c40,c41,c42,c43,c44,c45,c46,c47,c48;
  var c50,c51,c52,c53,c54,c55,c56,c57,c58;
  var c60,c61,c62,c63,c64,c65,c66,c67,c68;
  var c70,c71,c72,c73,c74,c75,c76,c77,c78;
  var c80,c81,c82,c83,c84,c85,c86,c87,c88;
  console.log($scope.b3);
 
  c00 = (
  ($scope.b3[0][0]*$scope.a[0][0])+
  (b01*$scope.a[1][0])+
  (b02*$scope.a[2][0])+
  (b03*$scope.a[3][0])+
  (b04*$scope.a[4][0])+
  (b05*$scope.a[5][0])/*+
  (b06*$scope.a[6][0])+
  (b07*$scope.a[7][0])+
  (b08*$scope.a[8][0])*/);
  console.log($scope.a[0][0]);
  console.log(b00*$scope.a[0][0]);
  console.log(c00);
  c01 = (
  (b00*$scope.a[0][1])+
  (b01*$scope.a[1][1])+
  (b02*$scope.a[2][1])+
  (b03*$scope.a[3][1])+
  (b04*$scope.a[4][1])+
  (b05*$scope.a[5][1])/*+
  (b06*$scope.a[6][1])+
  (b07*$scope.a[7][1])+
  (b08*$scope.a[8][1])*/);
  c02 = (
  (b00*$scope.a[0][2])+
  (b01*$scope.a[1][2])+
  (b02*$scope.a[2][2])+
  (b03*$scope.a[3][2])+
  (b04*$scope.a[4][2])+
  (b05*$scope.a[5][2])/*+
  (b06*$scope.a[6][2])+
  (b07*$scope.a[7][2])+
  (b08*$scope.a[8][2])*/);
  c10 = (
  (b10*$scope.a[0][0])+
  (b11*$scope.a[1][0])+
  (b12*$scope.a[2][0])+
  (b13*$scope.a[3][0])+
  (b14*$scope.a[4][0])+
  (b15*$scope.a[5][0])/*+
  (b16*$scope.a[6][0])+
  (b17*$scope.a[7][0])+
  (b18*$scope.a[8][0])*/);
  
  c11 = (
  (b10*$scope.a[0][1])+
  (b11*$scope.a[1][1])+
  (b12*$scope.a[2][1])+
  (b13*$scope.a[3][1])+
  (b14*$scope.a[4][1])+
  (b15*$scope.a[5][1])/*+
  (b16*$scope.a[6][1])+
  (b17*$scope.a[7][1])+
  (b18*$scope.a[8][1])*/);
  
  c12 = (
  (b10*$scope.a[0][2])+
  (b11*$scope.a[1][2])+
  (b12*$scope.a[2][2])+
  (b13*$scope.a[3][2])+
  (b14*$scope.a[4][2])+
  (b15*$scope.a[5][2])/*+
  (b16*$scope.a[6][2])+
  (b17*$scope.a[7][2])+
  (b18*$scope.a[8][2])*/);
  
  c20 = (
  (b20*$scope.a[0][0])+
  (b21*$scope.a[1][0])+
  (b22*$scope.a[2][0])+
  (b23*$scope.a[3][0])+
  (b24*$scope.a[4][0])+
  (b25*$scope.a[5][0])/*+
  (b26*$scope.a[6][0])+
  (b27*$scope.a[7][0])+
  (b28*$scope.a[8][0])*/);
  
  c21 = (
  (b20*$scope.a[0][1])+
  (b21*$scope.a[1][1])+
  (b22*$scope.a[2][1])+
  (b23*$scope.a[3][1])+
  (b24*$scope.a[4][1])+
  (b25*$scope.a[5][1])/*+
  (b26*$scope.a[6][1])+
  (b27*$scope.a[7][1])+
  (b28*$scope.a[8][1])*/);
  
  c22 = (
  (b20*$scope.a[0][2])+
  (b21*$scope.a[1][2])+
  (b22*$scope.a[2][2])+
  (b23*$scope.a[3][2])+
  (b24*$scope.a[4][2])+
  (b25*$scope.a[5][2])/*+
  (b26*$scope.a[6][2])+
  (b27*$scope.a[7][2])+
  (b28*$scope.a[8][2])*/);
  
  $scope.c=[
  [c00,c01,c02,c03,c04,c05,c06,c07,c08],
  [c10,c11,c12,c13,c14,c15,c16,c17,c18],
  [c20,c21,c22,c23,c24,c25,c26,c27,c28],
  [c30,c31,c32,c33,c34,c35,c36,c37,c38],
  [c40,c41,c42,c43,c44,c45,c46,c47,c48],
  [c50,c51,c52,c53,c54,c55,c56,c57,c58],
  [c60,c61,c62,c63,c64,c65,c66,c67,c68],
  [c70,c71,c72,c73,c74,c75,c76,c77,c78],
  [c80,c81,c82,c83,c84,c85,c86,c87,c88]
  ];
  console.log($scope.c);
  console.log(c00);
});
